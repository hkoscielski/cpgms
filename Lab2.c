#define _CRT_SECURE_NO_WARNINGS 

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

struct obraz
{
	char* nazwa;
	char* standard;
	int szerokosc;
	int wysokosc;	
	int glebia;
	int** szarosc;
};

void dealokujTablice(int** tab, int wiersze)
{
	int i = 0;
	for (i = 0; i < wiersze; i++)
	{
		free(tab[i]);
	}
	free(tab);
}

int** alokujTablice(int wiersze, int kolumny)
{
	int** tab = (int**)malloc(wiersze * sizeof(int*));
	if (tab == NULL)
	{
		perror("Blad podczas alokacji pamieci");
		exit(1);
	}
	else
	{
		int i;
		for (i = 0; i < kolumny; i++)
		{
			tab[i] = (int*)malloc(kolumny * sizeof(int));
			if (tab[i] == NULL)
			{
				perror("Blad podczas alokacji pamieci");				
				dealokujTablice(tab, i);
				exit(1);
			}
		}
	}	
	return tab;
}

void pokaz2Wym(int** tab, int wiersze, int kolumny)
{
	int i, j;
	for (i = 0; i < wiersze; i++)
	{
		for (j = 0; j < kolumny; j++)
		{
			printf("%3d ", tab[i][j]);
		}			
		puts("");
	}
}

struct obraz* wczytajObraz(char* nazwaPliku) 
{
	FILE* plik;	
	plik = fopen(nazwaPliku, "r");
	if (plik == NULL)
	{
		perror("Nie udalo si� otworzyc pliku");
	}
	else
	{
		struct obraz* pgm = (struct obraz*) malloc(sizeof(struct obraz));
		char* linia = (char*) malloc(256);
		int saKomentarze = 1;
		pgm->nazwa = nazwaPliku;
		pgm->standard = (char*) malloc(3);
		fscanf(plik, "%s", pgm->standard);		
		fscanf(plik, "%d", &(pgm->szerokosc));
		fscanf(plik, "%d", &(pgm->wysokosc));
		fscanf(plik, "%d", &(pgm->glebia));

		pgm->szarosc = alokujTablice(pgm->wysokosc, pgm->szerokosc);		
		int i, j;
		for (i = 0; i < pgm->wysokosc; i++)
		{
			for (j = 0; j < pgm->szerokosc; j++)
			{
				fscanf(plik, "%d", &(pgm->szarosc)[i][j]);				
			}			
		}
		
		fclose(plik);
		return pgm;
	}
	return NULL;
}

void zapiszObraz(char* nazwaPliku, struct obraz* obraz)
{
	FILE* plik;
	plik = fopen(nazwaPliku, "w");
	if (plik == NULL)
	{
		perror("Nie udalo si� otworzyc pliku");
	}
	else
	{				
		fprintf(plik, "%s\n", obraz->standard);			
		fprintf(plik, "%d %d\n", obraz->szerokosc, obraz->wysokosc);		
		fprintf(plik, "%d\n", obraz->glebia);		

		int i, j;
		for (i = 0; i < obraz->wysokosc; i++)
		{
			for (j = 0; j < obraz->szerokosc; j++)
			{
				fprintf(plik, "%d ", obraz->szarosc[i][j]);
			}
			fprintf(plik, "\n");
		}

		fclose(plik);
	}
}

char* podciag(char* tekst, int poczatek, int koniec) 
{
	char* nowyTekst;
	int i, j;

	nowyTekst = (char*) malloc(koniec - poczatek + 1);
	if (nowyTekst == NULL)
	{
		perror("Nie uda�o si� zaalokowa� pami�ci.\n");
		exit(1);
	}
	
	for (i = 0, j = poczatek; i < koniec - poczatek + 1; i++, j++)
	{
		nowyTekst[i] = tekst[j];				
	}
	nowyTekst[i] = '\0';

	return nowyTekst;
}

void pominKomentarze(char** tekst)
{
	int i;
	int znaleziono = 0;
	for (i = 0; (*tekst)[i] != '\0' && !znaleziono; i++)
	{
		if ((*tekst)[i] == '#')
		{
			znaleziono = 1;
			*tekst = podciag(*tekst, 0, i-1);
		}
	}	
}

void negatyw(struct obraz** obraz)
{
	int i, j;
	for (i = 0; i < (*obraz)->wysokosc; i++)
	{
		for (j = 0; j < (*obraz)->szerokosc; j++)
		{
			(*obraz)->szarosc[i][j] = (*obraz)->glebia - (*obraz)->szarosc[i][j];
		}		
	}
}

int main()
{	
	struct obraz* obraz = wczytajObraz("obrazy/merc.pgm");
	if (obraz != NULL)
	{
		/*puts("Oto nasz obraz:");
		printf("Nazwa: %s\n", obraz->nazwa);
		printf("Standard: %s\n", obraz->standard);
		printf("Szerokosc: %d\n", obraz->szerokosc);
		printf("Wysokosc: %d\n", obraz->wysokosc);
		printf("Glebia: %d\n", obraz->glebia);
		puts("Szarosc:");
		pokaz2Wym(obraz->szarosc, obraz->wysokosc, obraz->szerokosc);*/
		negatyw(&obraz);
		zapiszObraz("obrazy/nowy1.pgm", obraz);
		free(obraz);
	}	
	return 0;
}